<?php
  require_once('database.php');

  $action = isset($_POST['action']) ? $_POST['action'] : null;
  $result = array();

  if ($action) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $validate = true;

    if (!$email || !$password && $action == 'save') {
      $message = 'Email e senha são obrigatórios';
      $validate = false;
    } else if (!$email && $action == 'delete') {
      $message = 'Digite o email a ser removido.';
      $validate = false;
    }

    if ($validate === false) {
      $result = array(
        'status' => 'error',
        'message' => $message
      );
    }

    $message = null;

    if ($action == 'save' && $validate) {
      $sql = 'INSERT INTO users (email, password) VALUES ("' . $email . '", "' . $password . '")';
      $message = 'Usuário salvo com successo.';
    } else if ($action == 'delete' && $validate) {
      $sql = 'DELETE FROM users WHERE email = "' . $email . '"';
      $message = 'Usuário removido com successo.';
    }

    if ($validate) {
      if ($connection->query($sql) === true) {
        $result = array(
          'status' => 'success',
          'message' => $message
        );
      } else {
        $result = array(
          'status' => 'error',
          'message' => 'Erro ao salvar usuário.'
        );
      }
    }

  }
?>

<html>
<head>
   <title>php-crud</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 500px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
      .alert {
        margin: 0 auto;
        max-width: 508px;
        margin-bottom: 15px;
      }

    </style>
 
 </head>
<body>

    <div class="container">
      <?php if(isset($result['status'])) : ?>
        <div class="alert alert-block alert-<?php echo $result['status']; ?> fade in">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <p><?php echo $result['message']; ?></p>
        </div>
      <?php endif; ?>
    </div>

    <div class="container">

      <form class="form-signin" action="index.php" method="POST">
        <h2 class="form-signin-heading">Cadastre-se</h2>
        <input name="email" type="text" class="input-block-level" placeholder="Email address">
        <input name="password" type="password" class="input-block-level" placeholder="Password">
        <button name="action" value="save" class="btn btn-large btn-primary" type="submit">Salvar</button>
        <button name="action" value="delete" class="btn btn-large btn-primary" type="submit">Remover</button>
        <button class="btn btn-large btn-primary" type="reset">Incluir Novo</button>
      </form>

    </div> <!-- /container -->


</body>
</html>